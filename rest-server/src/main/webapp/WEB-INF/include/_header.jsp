<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title> TASK MANAGER </title>
</head>
<style>
    h1 {
        font-size: 1.6em;
    }
</style>
<body>

<table width="100%" height="100%" border="1" style="border-collapse: collapse;">

    <tr>
        <td height="35" width="200" nowrap="nowrap" align="center"><b>TASK MANAGER</b></td>
        <td width="100%" align="right">

            <a href="/tasks">TASKS</a>
            &nbsp&nbsp|&nbsp&nbsp
            <a href="/projects">PROJECTS</a>
            &nbsp&nbsp|&nbsp&nbsp
            <sec:authorize access="isAuthenticated()">
                <p>USER: <sec:authentication property="name"/></p>
                &nbsp&nbsp|&nbsp&nbsp
                <a href="/logout">LOGOUT</a>
            </sec:authorize>

            <sec:authorize access="!isAuthenticated()">
                <a href="/login">LOGIN</a>
            </sec:authorize>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="100%" valign="top" style="padding: 10px;">