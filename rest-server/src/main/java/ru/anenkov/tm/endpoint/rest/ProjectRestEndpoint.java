package ru.anenkov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.tm.exception.rest.NoSuchEntitiesException;
import ru.anenkov.tm.api.endpoint.IProjectRestEndpoint;
import org.springframework.web.bind.annotation.*;
import ru.anenkov.tm.service.ProjectService;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @PostMapping("/project")
    public ProjectDTO add(@Nullable @RequestBody ProjectDTO project) {
        project.setUserId(UserUtil.getUserId());
        projectService.addDTO(project);
        return project;
    }

    @Override
    @PutMapping("/project")
    public ProjectDTO update(@Nullable @RequestBody ProjectDTO project) {
        project.setUserId(UserUtil.getUserId());
        projectService.addDTO(project);
        return project;
    }

    @Override
    @GetMapping("/project/{id}")
    public @Nullable ProjectDTO findOneByIdEntity(@Nullable @PathVariable("id") String id) {
        return ProjectDTO.toProjectDTO(projectService.findProjectByIdAndUserId(id, UserUtil.getUserId()));
    }

    @Override
    @DeleteMapping("/project/{id}")
    public String removeOneById(@Nullable @PathVariable String id) {
        ProjectDTO project = ProjectDTO.toProjectDTO(projectService.findProjectByIdAndUserId(id, UserUtil.getUserId()));
        if (project == null)
            throw new NoSuchEntitiesException("project with id \"" + id + "\" does not exist!");
        projectService.removeProjectByIdAndUserId(id, UserUtil.getUserId());
        return "Project with id \"" + id + "\" was deleted successfully!";
    }

    @Override
    @DeleteMapping("/projects")
    public void removeAllProjects() {
        projectService.removeAllByUserId(UserUtil.getUserId());
    }

    @Override
    @Nullable
    @GetMapping("/projects")
    public List<ProjectDTO> getListByUserId() {
        return ProjectDTO.toProjectListDTO(projectService.findAllByUserId(UserUtil.getUserId()));
    }

    @Override
    @GetMapping("/projects/count")
    public long count() {
        return projectService.countByUserId(UserUtil.getUserId());
    }

}
