package ru.anenkov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.service.TaskService;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.model.Task;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

@Component
@WebService
public class TaskEndpoint {

    @Autowired
    private TaskService taskService;

    @WebMethod
    public void addDTOTask(@WebParam(name = "task") TaskDTO task) {
        taskService.addDTO(task);
    }


    @WebMethod
    public void addTask(@WebParam(name = "task") Task task) {
        taskService.add(task);
    }

    @WebMethod
    public void removeAllByUserIdAndProjectId(
            @Nullable @WebParam(name = "userId") final String userId,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        taskService.removeAllByUserIdAndProjectId(userId, projectId);
    }

    @WebMethod
    public List<TaskDTO> findAllByUserIdAndProjectId(
            @Nullable @WebParam(name = "userId") final String userId,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        return TaskDTO.toTaskListDTO(taskService.findAllByUserIdAndProjectId(userId, projectId));
    }

    @WebMethod
    public void removeTaskByUserIdAndProjectIdAndId(
            @Nullable @WebParam(name = "userId") final String userId,
            @Nullable @WebParam(name = "projectId") final String projectId,
            @Nullable @WebParam(name = "id") final String id
    ) {
        taskService.removeTaskByUserIdAndProjectIdAndId(userId, projectId, id);
    }

    @WebMethod
    public TaskDTO findTaskByUserIdAndProjectIdAndId(
            @Nullable @WebParam(name = "userId") final String userId,
            @Nullable @WebParam(name = "projectId") final String projectId,
            @Nullable @WebParam(name = "id") final String id
    ) {
        return TaskDTO.toTaskDTO(taskService.findTaskByUserIdAndProjectIdAndId(userId, projectId, id));
    }

    @WebMethod
    public TaskDTO findTaskByUserIdAndProjectIdAndName(
            @Nullable @WebParam(name = "userId") final String userId,
            @Nullable @WebParam(name = "projectId") final String projectId,
            @Nullable @WebParam(name = "name") final String name
    ) {
        return TaskDTO.toTaskDTO(taskService.findTaskByUserIdAndProjectIdAndId(userId, projectId, name));
    }

    @WebMethod
    public long countByUserIdAndProjectId(
            @Nullable @WebParam(name = "userId") final String userId,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        return taskService.countByUserIdAndProjectId(userId, projectId);
    }

}
