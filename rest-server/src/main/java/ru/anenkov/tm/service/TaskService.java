package ru.anenkov.tm.service;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.repository.TaskDtoRepository;
import ru.anenkov.tm.repository.TaskRepository;
import org.springframework.stereotype.Service;
import ru.anenkov.tm.api.service.ITaskService;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.TaskDTO;
import ru.anenkov.tm.model.Task;
import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.Optional;
import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Nullable
    @Autowired
    private TaskRepository taskRepository;

    @Nullable
    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @SneakyThrows
    public Task toTask(@Nullable final Optional<Task> taskDTO) {
        @Nullable Task newTask = new Task();
        newTask.setId(taskDTO.get().getId());
        newTask.setName(taskDTO.get().getName());
        newTask.setDescription(taskDTO.get().getDescription());
        newTask.setDateBegin(taskDTO.get().getDateBegin());
        newTask.setDateFinish(taskDTO.get().getDateFinish());
        newTask.setStatus(taskDTO.get().getStatus());
        newTask.setProject(taskDTO.get().getProject());
        return newTask;
    }

    @SneakyThrows
    public List<Task> toTaskList(@Nullable final List<Optional<Task>> taskOptDtoList) {
        List<Task> taskList = new ArrayList<>();
        for (Optional<Task> task : taskOptDtoList) {
            taskList.add(toTask(task));
        }
        return taskList;
    }

    @Transactional
    @Override
    public void removeAllByProjectId(@Nullable final String projectId) {
        taskRepository.removeAllByProjectId(projectId);
    }

    @Transactional
    public void removeAllByUserId(@Nullable final String userId) {
        taskRepository.removeAllByUserId(userId);
    }

    @Transactional
    public void removeAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) {
        taskRepository.removeAllByUserIdAndProjectId(userId, projectId);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Task> findAllByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyEntityException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Transactional(readOnly = true)
    public List<Task> findAllByUserId(@Nullable final String userId) {
        return taskRepository.findAllByUserId(userId);
    }

    @Transactional(readOnly = true)
    public List<Task> findAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Transactional(readOnly = true)
    @Override
    public Task findTaskByIdAndProjectId(@Nullable final String id, @Nullable final String projectId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyEntityException();
        return taskRepository.findTaskByIdAndProjectId(id, projectId);
    }

    @Transactional(readOnly = true)
    public Task findTaskByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        return taskRepository.findTaskByIdAndUserId(id, userId);
    }

    @Transactional(readOnly = true)
    public Task findTaskByUserIdAndProjectIdAndId(@Nullable final String userId, @Nullable final String projectId, @Nullable final String id) {
        return taskRepository.findTaskByUserIdAndProjectIdAndId(userId, projectId, id);
    }

    @Transactional(readOnly = true)
    public Task findTaskByUserIdAndProjectIdAndName(@Nullable String userId, @Nullable String projectId, @Nullable String name) {
        return taskRepository.findTaskByUserIdAndProjectIdAndName(userId, projectId, name);
    }

    @Transactional
    @Override
    public void removeTaskByIdAndProjectId(@Nullable final String id, @Nullable final String projectId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyEntityException();
        taskRepository.removeTaskByIdAndProjectId(id, projectId);
    }

    @Transactional
    @Override
    public void removeTaskByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyEntityException();
        taskRepository.removeTaskByIdAndUserId(id, userId);
    }

    @Transactional
    public void removeTaskByUserIdAndProjectIdAndId(@Nullable final String userId, @Nullable final String projectId, @Nullable final String id) {
        taskRepository.removeTaskByUserIdAndProjectIdAndId(userId, projectId, id);
    }

    @Transactional(readOnly = true)
    @Override
    public long countByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyEntityException();
        return taskRepository.countByProjectId(projectId);
    }

    @Transactional(readOnly = true)
    public long countByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return taskRepository.countByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @Transactional
    public Task save(@Nullable final Task task) {
        if (task == null) throw new EmptyEntityException();
        return taskRepository.save(task);
    }

    @Transactional
    public void addDTO(
            @Nullable final TaskDTO task
    ) {
        if (task == null) throw new EmptyEntityException();
        taskDtoRepository.save(task);
    }

    @Transactional
    public void add(
            @Nullable final Task task
    ) {
        if (task == null) throw new EmptyEntityException();
        taskRepository.save(task);
    }

    @Transactional(readOnly = true)
    public Task findOneByIdEntity(
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return toTask(taskRepository.findById(id));
    }

    @Transactional
    public void removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<Task> getList() {
        return taskRepository.findAll();
    }

    @Transactional
    public void removeAllTasks() {
        taskRepository.deleteAll();
    }

    @Transactional(readOnly = true)
    public long count() {
        return taskRepository.count();
    }

}
