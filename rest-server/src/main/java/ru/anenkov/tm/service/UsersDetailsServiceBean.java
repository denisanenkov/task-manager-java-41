package ru.anenkov.tm.service;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import ru.anenkov.tm.enumerated.RoleType;
import ru.anenkov.tm.model.Role;
import ru.anenkov.tm.repository.UserRepository;
import org.springframework.stereotype.Service;
import ru.anenkov.tm.dto.CustomUser;
import ru.anenkov.tm.model.User;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service("userDetailsService")
public class UsersDetailsServiceBean implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public boolean saveUser(User user) {
        User userFromDB = userRepository.findByLogin(user.getLogin());
        if (userFromDB != null) return false;
        user.setRoles(Collections.singleton(new Role(1L, "ROLE_USER")));
        user.setPasswordHash(bCryptPasswordEncoder.encode(user.getPasswordHash()));
        userRepository.save(user);
        return true;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(user.getRoles().toArray().toString())
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    public void initUsers() {
        if (userRepository.count() > 0) return;
        create("admin", "admin");
        create("test", "test");
        create("1", "1");
    }

    public void create(final String login, final String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        user.setRoles(Collections.singleton(new Role(1L, "ROLE_USER")));
        userRepository.save(user);
    }

}
