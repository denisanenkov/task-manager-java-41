package ru.anenkov.tm.controller;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.security.core.Authentication;
import ru.anenkov.tm.model.User;
import ru.anenkov.tm.repository.UserRepository;

import javax.annotation.Resource;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @GetMapping(value = "/login", produces = "application/json")
    public boolean login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication =
                    authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return (authentication.isAuthenticated());
        } catch (final Exception ex) {
            return false;
        }
    }

    @GetMapping(value = "/session", produces = "application/json")
    public Authentication session() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @GetMapping(value = "/user", produces = "application/json")
    public User user(@AuthenticationPrincipal(errorOnInvalidType = true) final User user) {
        return user;
    }

    @GetMapping(value = "/profile", produces = "application/json")
    public ru.anenkov.tm.model.User profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return userRepository.findByLogin(username);
    }

}
