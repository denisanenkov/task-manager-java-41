package ru.anenkov.tm.exception.empty;

import ru.anenkov.tm.exception.AbstractException;

public class EmptyTimestampSessionException extends AbstractException {

    public EmptyTimestampSessionException() {
        super("Error! Session timestamp is empty!");
    }

}
