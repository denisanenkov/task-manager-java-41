package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskService {

    void removeAllByProjectId(@Nullable String projectId);

    List<Task> findAllByProjectId(@Nullable String projectId);

    Task findTaskByIdAndProjectId(@Nullable String id, @Nullable String projectId);

    void removeTaskByIdAndProjectId(@Nullable String id, @Nullable String projectId);

    void removeTaskByIdAndUserId(@Nullable final String id, @Nullable final String userId);

    long countByProjectId(@Nullable String projectId);

    Task save(@Nullable Task task);

}
