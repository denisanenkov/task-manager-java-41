package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.ProjectDTO;
import ru.anenkov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    @Nullable
    ProjectDTO findOneByIdEntity(@Nullable String id);

    void removeOneById(@Nullable String id);

    void removeAllProjects();

    void add(@Nullable Project project);

    void addDTO(@Nullable ProjectDTO project);

    @Nullable
    List<Project> getList();

    long count();

    public void removeAllByUserId(@Nullable String userId);

    @Nullable
    public List<Project> findAllByUserId(@Nullable String userId);

    @Nullable
    public Project findProjectByIdAndUserId(@Nullable String id, @Nullable String userId);

    public void removeProjectByIdAndUserId(@Nullable String id, @Nullable String userId);

    public long countByUserId(@Nullable String userId);

    Project findProjectByUserIdAndName(@Nullable String userId, @Nullable String name);

}