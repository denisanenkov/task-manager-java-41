package ru.anenkov.tm.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.anenkov.tm.model.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String> {

    User findByLogin(String login);

}
