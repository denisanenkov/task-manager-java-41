package ru.anenkov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.anenkov.tm.dto.TaskDTO;

public interface TaskDtoRepository extends JpaRepository<TaskDTO, String> {
}
