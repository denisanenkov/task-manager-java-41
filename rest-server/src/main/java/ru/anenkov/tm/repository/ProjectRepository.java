package ru.anenkov.tm.repository;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.Project;

import java.util.*;

public interface ProjectRepository extends JpaRepository<Project, String> {

    @Transactional
    void removeAllByUserId(@Nullable @Param("userId") final String userId);

    @Transactional(readOnly = true)
    List<Project> findAllByUserId(@Nullable @Param("userId") final String userId);

    @Transactional(readOnly = true)
    Project findProjectByIdAndUserId(
            @Nullable @Param("id") final String id,
            @Nullable @Param("userId") final String userId);

    @Transactional
    Project findProjectByUserIdAndName(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("name") final String name);

    @Transactional
    void removeProjectByIdAndUserId(
            @Nullable @Param("id") final String id,
            @Nullable @Param("userId") final String userId);

    @Transactional(readOnly = true)
    long countByUserId(@Nullable @Param("userId") final String userId);

}
