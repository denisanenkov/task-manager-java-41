package ru.anenkov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.repository.query.Param;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.Task;

import java.util.*;

public interface TaskRepository extends JpaRepository<Task, String> {

    @Transactional
    void removeAllByProjectId(@Nullable @Param("projectId") final String projectId);

    @Transactional
    void removeAllByUserId(@Nullable @Param("userId") final String userId);

    @Transactional
    void removeAllByUserIdAndProjectId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId);

    @Transactional(readOnly = true)
    List<Task> findAllByProjectId(
            @Nullable @Param("projectId") final String projectId);

    @Transactional(readOnly = true)
    List<Task> findAllByUserId(
            @Nullable @Param("userId") final String userId);

    @Transactional(readOnly = true)
    List<Task> findAllByUserIdAndProjectId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId);

    @Transactional(readOnly = true)
    Task findTaskByIdAndProjectId(
            @Nullable @Param("id") final String id,
            @Nullable @Param("projectId") final String projectId);

    @Transactional(readOnly = true)
    Task findTaskByIdAndUserId(
            @Nullable @Param("id") final String id,
            @Nullable @Param("userId") final String userId);

    @Transactional(readOnly = true)
    Task findTaskByUserIdAndProjectIdAndId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId,
            @Nullable @Param("id") final String id);

    @Transactional(readOnly = true)
    Task findTaskByUserIdAndProjectIdAndName(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId,
            @Nullable @Param("name") final String name);

    @Transactional
    void removeTaskByIdAndProjectId(
            @Nullable @Param("id") final String id,
            @Nullable @Param("projectId") final String projectId);

    @Transactional
    void removeTaskByIdAndUserId(
            @Nullable @Param("id") final String id,
            @Nullable @Param("projectId") final String projectId);

    @Transactional
    void removeTaskByUserIdAndProjectIdAndId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId,
            @Nullable @Param("id") final String id);

    @Transactional(readOnly = true)
    long countByProjectId(@Nullable @Param("projectId") final String projectId);

    @Transactional(readOnly = true)
    long countByUserIdAndProjectId(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("projectId") final String projectId);

}
