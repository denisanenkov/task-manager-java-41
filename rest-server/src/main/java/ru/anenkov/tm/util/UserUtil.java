package ru.anenkov.tm.util;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.CustomUser;

public class UserUtil {

    @Nullable
    public static String getUserId() {
        @Nullable final Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        @Nullable final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException("");
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException("");
        @Nullable final CustomUser customUser = (CustomUser) principal;
        return customUser.getUserId();
    }

}
