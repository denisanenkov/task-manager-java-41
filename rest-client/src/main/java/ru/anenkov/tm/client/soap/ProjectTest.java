package ru.anenkov.tm.client.soap;

import ru.anenkov.tm.util.CredentialsUtil;
import ru.anenkov.tm.util.TerminalUtil;
import ru.anenkov.tm.endpoint.soap.*;

import java.util.List;

import static ru.anenkov.tm.client.AbstractClient.*;

public class ProjectTest {

    final AuthEndpointService authEndpointService = new AuthEndpointService();
    final AuthEndpoint authEndpoint = authEndpointService.getAuthEndpointPort();

    final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    public static void main(String[] args) {
        ProjectTest projectTest = new ProjectTest();
        projectTest.update();
    }

    public void findAll() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            setMaintain(projectEndpoint);
            setListCookieRowRequest(projectEndpoint, session);
            System.out.println("PROJECT LIST: ");
            for (ProjectDTO projectDTO : projectEndpoint.getList()) {
                System.out.println(projectDTO);
            }
            authEndpoint.logout();
        } else System.out.println("LOGIN FAIL");
    }

    public void findByName() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            setMaintain(projectEndpoint);
            setListCookieRowRequest(projectEndpoint, session);
            System.out.print("ENTER NAME OF PROJECT: ");
            String name = TerminalUtil.nextLine();
            System.out.println(projectEndpoint.findProjectByUserIdAndName(authEndpoint.profile().getId(), name));
            authEndpoint.logout();
        } else System.out.println("LOGIN FAIL");
    }

    public void findById() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            setMaintain(projectEndpoint);
            setListCookieRowRequest(projectEndpoint, session);
            System.out.print("ENTER ID OF PROJECT: ");
            String id = TerminalUtil.nextLine();
            System.out.println(projectEndpoint.findOneByIdEntity(id));
            authEndpoint.logout();
        } else System.out.println("LOGIN FAIL");
    }

    public void removeById() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            setMaintain(projectEndpoint);
            setListCookieRowRequest(projectEndpoint, session);
            long beginCount = projectEndpoint.count();
            System.out.print("ENTER ID OF PROJECT: ");
            String id = TerminalUtil.nextLine();
            projectEndpoint.removeOneById(id);
            long endCount = projectEndpoint.count();
            if (beginCount - endCount == 1) System.out.println("DELETE PROJECT WITH ID " + id + " SUCCESS");
            authEndpoint.logout();
        } else System.out.println("LOGIN FAIL");
    }

    public void removeAll() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            setMaintain(projectEndpoint);
            setListCookieRowRequest(projectEndpoint, session);
            projectEndpoint.removeAllProjects(authEndpoint.profile().getId());
            long endCount = projectEndpoint.count();
            if (endCount == 0) System.out.println("DELETE PROJECTS SUCCESS");
            authEndpoint.logout();
        } else System.out.println("LOGIN FAIL");
    }

    public void add() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            setMaintain(projectEndpoint);
            setListCookieRowRequest(projectEndpoint, session);
            System.out.print("ENTER NAME: ");
            String name = TerminalUtil.nextLine();
            System.out.print("ENTER DESCRIPTION: ");
            String description = TerminalUtil.nextLine();
            ProjectDTO projectDTO = new ProjectDTO();
            projectDTO.setName(name);
            projectDTO.setDescription(description);
            projectDTO.setUserId(authEndpoint.profile().getId());
            long beginCount = projectEndpoint.count();
            projectEndpoint.addProject(projectDTO);
            long endCount = projectEndpoint.count();
            if (endCount - beginCount == 1) System.out.println("ADD PROJECT SUCCESS");
            authEndpoint.logout();
        } else System.out.println("LOGIN FAIL");
    }

    public void update() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            setMaintain(projectEndpoint);
            setListCookieRowRequest(projectEndpoint, session);
            System.out.print("ENTER PROJECT NAME TO UPDATE IT: ");
            String name = TerminalUtil.nextLine();
            ProjectDTO projectDTO = projectEndpoint.findProjectByUserIdAndName(authEndpoint.profile().getId(), name);
            System.out.print("ENTER NEW PROJECT NAME: ");
            String newName = TerminalUtil.nextLine();
            System.out.print("ENTER NEW DESCRIPTION: ");
            String description = TerminalUtil.nextLine();
            projectDTO.setName(newName);
            projectDTO.setDescription(description);
            projectDTO.setUserId(authEndpoint.profile().getId());
            long beginCount = projectEndpoint.count();
            projectEndpoint.addProject(projectDTO);
            long endCount = projectEndpoint.count();
            if (endCount - beginCount == 0) System.out.println("UPDATE PROJECT SUCCESS");
            authEndpoint.logout();
        } else System.out.println("LOGIN FAIL");
    }

}
