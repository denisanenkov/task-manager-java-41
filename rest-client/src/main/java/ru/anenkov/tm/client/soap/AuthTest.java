package ru.anenkov.tm.client.soap;

import org.springframework.stereotype.Component;
import ru.anenkov.tm.util.TerminalUtil;
import ru.anenkov.tm.endpoint.soap.*;

import java.util.List;

import static ru.anenkov.tm.client.AbstractClient.*;

@Component
public class AuthTest {

    final AuthEndpointService authEndpointService = new AuthEndpointService();
    final AuthEndpoint authEndpoint = authEndpointService.getAuthEndpointPort();

    public static void main(String[] args) {
        AuthTest authTest = new AuthTest();
        authTest.profile();
    }

    public void login() {
        setMaintain(authEndpoint);
        System.out.print("LOGIN: ");
        String login = TerminalUtil.nextLine();
        System.out.print("PASSWORD: ");
        String password = TerminalUtil.nextLine();
        if (authEndpoint.login(login, password)) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            authEndpoint.logout();
        } else System.out.println("LOGIN FAIL");
    }

    public void profile() {
        setMaintain(authEndpoint);
        System.out.print("LOGIN: ");
        String login = TerminalUtil.nextLine();
        System.out.print("PASSWORD: ");
        String password = TerminalUtil.nextLine();
        if (authEndpoint.login(login, password)) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            System.out.println("PROFILE: \n" + authEndpoint.profile().getId() + " - ID;\n" +
                    authEndpoint.profile().getLogin() + " - LOGIN;\n" +
                    authEndpoint.profile().getPasswordHash() + " - PASSWORD HASH;\n");
        } else System.out.println("LOGIN FAIL");
    }

}
