package ru.anenkov.tm.client.soap;

import org.springframework.stereotype.Component;
import ru.anenkov.tm.endpoint.soap.*;
import ru.anenkov.tm.util.TerminalUtil;
import ru.anenkov.tm.util.CredentialsUtil;

import java.util.List;
import javax.xml.ws.soap.SOAPFaultException;

import static ru.anenkov.tm.client.AbstractClient.*;

@Component
public class TaskTest {

    final AuthEndpointService authEndpointService = new AuthEndpointService();
    final AuthEndpoint authEndpoint = authEndpointService.getAuthEndpointPort();

    final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    final TaskEndpointService taskEndpointService = new TaskEndpointService();
    final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    public static void main(String[] args) {
        TaskTest taskTest = new TaskTest();
        taskTest.update();
    }

    public void findAll() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            System.out.print("ENTER NAME OF PROJECT TO GET ALL TASKS: ");
            String name = TerminalUtil.nextLine();
            ProjectDTO projectDTO;
            try {
                projectDTO = projectEndpoint.findProjectByUserIdAndName
                        (authEndpoint.profile().getId(), name);
            } catch (SOAPFaultException ex) {
                System.out.println("NOT FOUND PROJECT! CHECK THE ENTERED DATA");
                authEndpoint.logout();
                return;
            }
            System.out.println("TASK LIST: ");
            authEndpoint.logout();
            try {
                for (TaskDTO taskDTO : taskEndpoint.findAllByUserIdAndProjectId(authEndpoint.profile().getId(), projectDTO.getId())) {
                    System.out.println(taskDTO);
                }
            } catch (SOAPFaultException ex) {
                System.out.println("NOT FOUND");
                authEndpoint.logout();
                return;
            }
        } else System.out.println("LOGIN FAIL");
    }

    public void findById() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            System.out.print("ENTER NAME OF PROJECT TO GET TASK: ");
            String name = TerminalUtil.nextLine();
            ProjectDTO projectDTO;
            try {
                projectDTO = projectEndpoint.findProjectByUserIdAndName
                        (authEndpoint.profile().getId(), name);
            } catch (SOAPFaultException ex) {
                System.out.println("NOT FOUND PROJECT! CHECK THE ENTERED DATA");
                authEndpoint.logout();
                return;
            }
            System.out.print("ENTER ID OF TASK: ");
            String id = TerminalUtil.nextLine();
            try {
                taskEndpoint.findTaskByUserIdAndProjectIdAndId
                        (authEndpoint.profile().getId(), projectDTO.getId(), id);
            } catch (SOAPFaultException ex) {
                System.out.println("NOT FOUND");
                authEndpoint.logout();
                return;
            }
        } else System.out.println("LOGIN FAIL");
    }

    public void findByName() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            System.out.print("ENTER NAME OF PROJECT TO GET TASK: ");
            String name = TerminalUtil.nextLine();
            ProjectDTO projectDTO;
            try {
                projectDTO = projectEndpoint.findProjectByUserIdAndName
                        (authEndpoint.profile().getId(), name);
            } catch (SOAPFaultException ex) {
                System.out.println("NOT FOUND PROJECT! CHECK THE ENTERED DATA");
                authEndpoint.logout();
                return;
            }
            System.out.print("ENTER NAME OF TASK: ");
            String nameTask = TerminalUtil.nextLine();
            try {
                taskEndpoint.findTaskByUserIdAndProjectIdAndId
                        (authEndpoint.profile().getId(), projectDTO.getId(), nameTask);
            } catch (SOAPFaultException ex) {
                System.out.println("NOT FOUND! CHECK THE ENTERED DATA");
                authEndpoint.logout();
                return;
            }
        } else System.out.println("LOGIN FAIL");
    }

    public void removeById() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            System.out.print("ENTER NAME OF PROJECT TO REMOVE TASK: ");
            String name = TerminalUtil.nextLine();
            ProjectDTO projectDTO;
            try {
                projectDTO = projectEndpoint.findProjectByUserIdAndName
                        (authEndpoint.profile().getId(), name);
            } catch (SOAPFaultException ex) {
                System.out.println("NOT FOUND PROJECT! CHECK THE ENTERED DATA");
                authEndpoint.logout();
                return;
            }
            System.out.print("ENTER ID OF TASK: ");
            String id = TerminalUtil.nextLine();
            try {
                taskEndpoint.removeTaskByUserIdAndProjectIdAndId
                        (authEndpoint.profile().getId(), projectDTO.getId(), id);
            } catch (SOAPFaultException ex) {
                System.out.println("NOT FOUND! CHECK THE ENTERED DATA");
                authEndpoint.logout();
                return;
            }
        } else System.out.println("LOGIN FAIL");
    }

    public void removeAll() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            System.out.print("ENTER NAME OF PROJECT TO REMOVE TASK: ");
            String name = TerminalUtil.nextLine();
            ProjectDTO projectDTO;
            try {
                projectDTO = projectEndpoint.findProjectByUserIdAndName
                        (authEndpoint.profile().getId(), name);
            } catch (SOAPFaultException ex) {
                System.out.println("NOT FOUND PROJECT! CHECK THE ENTERED DATA");
                authEndpoint.logout();
                return;
            }
            taskEndpoint.removeAllByUserIdAndProjectId
                    (authEndpoint.profile().getId(), projectDTO.getId());
            if (taskEndpoint.countByUserIdAndProjectId(authEndpoint.profile().getId(), projectDTO.getId()) == 0)
                System.out.println("DELETE SUCCESS");
        } else System.out.println("LOGIN FAIL");
    }

    public void save() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            System.out.print("ENTER NAME OF PROJECT TO ADD TASK: ");
            String name = TerminalUtil.nextLine();
            ProjectDTO projectDTO;
            try {
                projectDTO = projectEndpoint.findProjectByUserIdAndName
                        (authEndpoint.profile().getId(), name);
            } catch (SOAPFaultException ex) {
                System.out.println("NOT FOUND PROJECT! CHECK THE ENTERED DATA");
                authEndpoint.logout();
                return;
            }
            TaskDTO task = new TaskDTO();
            task.setProjectId(projectDTO.getId());
            task.setUserId(authEndpoint.profile().getId());
            System.out.print("ENTER NAME: ");
            String nameTask = TerminalUtil.nextLine();
            System.out.print("ENTER DESCRIPTION: ");
            String descriptionTask = TerminalUtil.nextLine();
            task.setName(nameTask);
            task.setDescription(descriptionTask);
            long beginCount = taskEndpoint.countByUserIdAndProjectId(authEndpoint.profile().getId(), projectDTO.getId());
            taskEndpoint.addDTOTask(task);
            if (taskEndpoint.countByUserIdAndProjectId(authEndpoint.profile().getId(), projectDTO.getId()) - beginCount == 1) {
                System.out.println("ADDING SUCCESSFUL");
            } else {
                System.out.println("ADD TASK FAIL");
            }

        } else System.out.println("LOGIN FAIL");
    }

    public void update() {
        setMaintain(authEndpoint);
        String[] credentials = CredentialsUtil.getCredentials();
        if (authEndpoint.login(credentials[0], credentials[1])) {
            System.out.println("LOGIN SUCCESS");
            final List<String> session = getListSetCookieRow(authEndpoint);
            System.out.print("ENTER NAME OF PROJECT TO ADD TASK: ");
            String name = TerminalUtil.nextLine();
            ProjectDTO projectDTO;
            try {
                projectDTO = projectEndpoint.findProjectByUserIdAndName
                        (authEndpoint.profile().getId(), name);
            } catch (SOAPFaultException ex) {
                System.out.println("NOT FOUND PROJECT! CHECK THE ENTERED DATA");
                authEndpoint.logout();
                return;
            }
            System.out.print("ENTER NAME OF TASK TO UPDATE: ");
            String nameToFind = TerminalUtil.nextLine();
            TaskDTO task = taskEndpoint.findTaskByUserIdAndProjectIdAndName(authEndpoint.profile().getId(), projectDTO.getId(), nameToFind);
            System.out.print("ENTER NEW NAME: ");
            String nameTask = TerminalUtil.nextLine();
            System.out.print("ENTER NEW DESCRIPTION: ");
            String descriptionTask = TerminalUtil.nextLine();
            task.setName(nameTask);
            task.setDescription(descriptionTask);
            long beginCount = taskEndpoint.countByUserIdAndProjectId(authEndpoint.profile().getId(), projectDTO.getId());
            taskEndpoint.addDTOTask(task);
            if (taskEndpoint.countByUserIdAndProjectId(authEndpoint.profile().getId(), projectDTO.getId()) - beginCount == 0) {
                System.out.println("ADDING SUCCESSFUL");
            } else {
                System.out.println("ADD TASK FAIL");
            }
        } else System.out.println("LOGIN FAIL");
    }

}
