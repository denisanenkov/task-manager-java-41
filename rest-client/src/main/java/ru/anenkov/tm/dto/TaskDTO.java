package ru.anenkov.tm.dto;

import org.springframework.format.annotation.DateTimeFormat;
import ru.anenkov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class TaskDTO {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    private String projectId;

    public TaskDTO(String name) {
        this.name = name;
    }

    public TaskDTO(String name, String description, Status status, Date dateBegin, Date dateFinish) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateFinish = dateFinish;
    }

    public TaskDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public TaskDTO() {
    }

    public TaskDTO(String id, String name, String description, Status status, Date dateBegin, Date dateFinish, String projectId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.status = status;
        this.dateBegin = dateBegin;
        this.dateFinish = dateFinish;
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "\nTASK: \n" +
                "ID='" + id + '\'' +
                ";\nNAME='" + name + '\'' +
                ";\nDESCRIPTION='" + description +
                ";\nDATE BEGIN ='" + dateBegin + '\'' +
                ";\nDATE END ='" + dateFinish + '\'' + "'\n";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
